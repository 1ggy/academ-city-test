<?php


namespace App\Controller;

use App\Entity\Player;
use App\Service\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Twig\Attribute\Template;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Class SecretSantaController
 * @package App\Controller
 */
class SecretSantaController extends AbstractController
{
    /**
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @return array
     */
    #[Route('/secret-santa', name: 'index')]
    #[Template('secret-santa/index.html.twig')]
    public function index(Request $request, ManagerRegistry $doctrine)
    {
        $player = new Player();
        $form = $this->createFormBuilder($player)
            ->add('email', EmailType::class)
            ->add('fio', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $doctrine->getManager();
            $player = $form->getData();
            $em->persist($player);
            $em->flush();
            return $this->redirect($request->getUri());
        }

        $players = $doctrine->getRepository(Player::class)->findAll();

        return [
            'players' => $players,
            'form' => $form
        ];
    }

    /**
     * @param Player $player
     * @param ManagerRegistry $doctrine
     */
    #[Route('/secret-santa/delete/{id}', name: 'delete')]
    public function delete(Player $player, ManagerRegistry $doctrine)
    {
        $em = $doctrine->getManager();
        $em->remove($player);
        $em->flush();
        return $this->redirectToRoute('index');
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param PlayerService $playerService
     * @return
     */
    #[Route('/secret-santa/shake', name: 'shake')]
    public function shake(ManagerRegistry $doctrine, PlayerService $playerService)
    {
        //сбрасываем старые значения
        $doctrine->getRepository(Player::class)->clearAllAnotherPlayers();

        $players = $doctrine->getRepository(Player::class)->findAll();
        $playerService->setAnotherPlayers($players);
        $em = $doctrine->getManager();
        foreach ($players as $player) {
            $em->persist($player);
        }
        $em->flush();

        return $this->redirectToRoute('index');
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param MailerInterface $mailer
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    #[Route('/secret-santa/send-letters', name: 'send-letters')]
    public function sendLetters(ManagerRegistry $doctrine, MailerInterface $mailer)
    {
        $playerRepository = $doctrine->getRepository(Player::class);
        if (!$playerRepository->allPlayersPrepared()) {
            $this->addFlash(
                'warning',
                'Сначала нажмите кнопку Перемешать'
            );
            return $this->redirectToRoute('index');
        }

        $players = $playerRepository->findAll();
        /** @var Player $player */
        foreach ($players as $player) {
            $email = (new Email())
                ->to($player->getEmail())
                ->subject('Игра Секретный Санта')
                ->text('Вы участвуете в игре Секретный Санта. Вам нужно подготовить подарок для ' . $player->getAnotherPlayer()->getFio() . ' - ' . $player->getAnotherPlayer()->getEmail());

            $mailer->send($email);
        }

        $this->addFlash(
            'success',
            'Рассылка писем запущена'
        );
        return $this->redirectToRoute('index');
    }
}