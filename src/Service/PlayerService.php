<?php


namespace App\Service;

use App\Entity\Player;

/**
 * Class PlayerService
 * @package App\Service
 */
class PlayerService
{
    /**
     * перемешиваем массив пользователей и назначаем их сантами по кругу
     * @param Player[] $players
     * @return Player[] array
     */
    public function setAnotherPlayers(array &$players): void
    {
        shuffle($players);
        foreach ($players as $key => &$player) {
            $next = $players[++$key] ?? null;
            if ($next) {
                $player->setAnotherPlayer($next);
            } else {
                $player->setAnotherPlayer($players[0]);
            }
        }
    }
}