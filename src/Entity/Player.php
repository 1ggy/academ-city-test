<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PlayerRepository::class)]
#[UniqueEntity('email')]
/**
 * Class Player
 * @package App\Entity
 */
class Player
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    /** @var int|null  */
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\Email(message: 'The email {{ value }} is not a valid email.')]
    /** @var string|null  */
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    /** @var string|null  */
    private ?string $fio = null;

    #[ORM\OneToOne(targetEntity: self::class)]
    #[JoinColumn(name: 'another_player_id', referencedColumnName: 'id')]
    /** @var $this|null  */
    private ?self $anotherPlayer = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFio(): ?string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     * @return $this
     */
    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * @return $this|null
     */
    public function getAnotherPlayer(): ?self
    {
        return $this->anotherPlayer;
    }

    /**
     * @param Player|null $anotherPlayer
     * @return $this
     */
    public function setAnotherPlayer(?self $anotherPlayer): self
    {
        $this->anotherPlayer = $anotherPlayer;

        return $this;
    }
}
